# Download Gitlab artifacts

## Installation

1. Install dependencies:
	* `python3` + `pip3`: `sudo apt install python3-dev python3-pip`
	* [`python-gitlab`](https://github.com/python-gitlab/python-gitlab) + [`click`](http://click.pocoo.org/5/): sudo pip3 install python-gitlab click
1. Clone the repo: `git clone git@gitlab.com:kylecarbon/gitlab_artifacts.git`
1. Set up configuration for `python-gitlab`: copy `python-gitlab.cfg.template` to `python-gitlab.cfg` and insert your gitlab API token. If you're not on gitlab.com, replace the URL as well.

## Usage

`./get_artifacts.py -h`