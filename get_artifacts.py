#!/usr/bin/env python3

import gitlab
import os
import getpass
import shutil
import logging
import datetime
import zipfile
import click

# look for pipeline that passed
def find_passing_pipeline(pipelines):
    for pipeline in pipelines:
        status = pipeline.attributes['status']
        if status == "success":
            temp_string = """Found successful pipeline:\n\tCommit: {}\n\tPipeline ID: {}\n\tRef: {}"""
            commit = pipeline.attributes['sha']
            pipeline_id = pipeline.attributes['id']
            ref = pipeline.attributes['ref']
            arg_string = temp_string.format(commit, pipeline_id, ref)
            logging.debug(arg_string)
            return pipeline

    return None

# find job by name
def find_job(jobs, job_name="docs"):
    for job in jobs.list():
        i_job_name = job.attributes["name"]
        if i_job_name == job_name:
            return job
    return None

# download artifacts as zip file, then extract to dest_dir
#   this assumes artifacts zip file contains a folder titled "public"
def get_artifacts(job, src_dir, dest_dir):
    download_dir = os.path.join(os.path.expanduser("~"), "Downloads")
    todays_date = datetime.datetime.now()
    zip_name = "gitlab_artifacts_" + todays_date.strftime("%Y%m%d_%H%M%S") + ".zip"
    zip_file = os.path.join(download_dir, zip_name)
    try:
        logging.info("Saving zip file to: {}".format(download_dir))
        with open(zip_file, "wb") as f:
            job.artifacts(streamed=True, action=f.write)
    except:
        logging.error("Could not download zip file. Exiting.")
        return

    logging.info("Unzipping artifacts...")
    try:
        zip_ref = zipfile.ZipFile(zip_file, 'r')
        zip_ref.extractall(download_dir)
        zip_ref.close()
        logging.info("\tsuccess!")
    except:
        logging.error("Failed to unzip artifacts.")


    # After unzipping, artifact contents will be found here
    artifacts_dir = os.path.join(download_dir, src_dir)

    if os.path.isfile(zip_file):
        logging.info("Deleting zip file.")
        os.remove(zip_file)
    else:
        logging.error("Could not find zip file: {}".format(zip_file))
        return

    if os.path.exists(dest_dir):
        logging.info("Destination directory contains contents -- deleting them.")
        shutil.rmtree(dest_dir)

    try:
        logging.info("Moving artifacts to destination directory.")
        shutil.move(artifacts_dir, dest_dir)
        logging.info("Completed successfully.")
    except:
        logging.error("Failed to move artifacts to destination directory. Exiting.")

CONTEXT_SETTINGS = dict(
    help_option_names=['-h', '--help'],
    max_content_width=100
)

@click.command(context_settings=CONTEXT_SETTINGS)
@click.option(
    '-p', '--project-id',
    help="Gitlab project ID (shown on homepage of your gitlab repository).",
    metavar="<project_id>"
)
@click.option(
    '-j', '--job-name',
    help="The job name from which to pull artifacts.",
    metavar="<job_name>"
)
@click.option(
    '-r', '--pipeline-ref',
    help="The <ref> to use to filter pipelines. Defaults to <master>.",
    metavar="<pipeline_ref>"
)
@click.option(
    '-s', '--src-dir',
    help="Source directory within the artifacts zip file to search.",
    metavar="<src_dir>",
)
@click.option(
    '-d', '--dest-dir',
    help="Destination directory to save artifacts.",
    metavar="<dest_dir>",
)
def cli(project_id, job_name, pipeline_ref, src_dir, dest_dir):
    """
    This searches for a successful pipeline, downloads artifacts from the job, then unzips them to a user-specified location.

    ASSUMES that artifact contents are located in a folder titled "public".
    """
    # set up logging
    dirname = os.path.dirname(__file__)
    log_dir = os.path.join(dirname, "logs")
    if not os.path.exists(log_dir):
        os.mkdir(log_dir)
    todays_date = datetime.datetime.now()
    log_name = "artifacts_" + todays_date.strftime("%Y%m%d_%H%M%S") + ".log"
    log_file = os.path.join(log_dir, log_name)
    if os.path.exists(log_file):
        os.remove(log_file)
    logging.basicConfig(filename=log_file,
                        format='%(asctime)s %(message)s',
                        level=logging.DEBUG)
    print("Log file can be found at {}.".format(log_file))
    temp_string = """get_artifacts.py called with arguments:\n\tProject ID: {}\n\tJob name: {}\n\tPipeline reference: {}\n\tSource directory: {}\n\tDestination directory: {}"""
    arg_string = temp_string.format(project_id, job_name, pipeline_ref, src_dir, dest_dir)
    logging.debug(arg_string)

    if not project_id:
        logging.error("No project ID given. Exiting.")
        return

    if not job_name:
        logging.error("No job name given. Exiting.")
        return

    if not pipeline_ref:
        logging.info("No pipeline reference given -- defaulting to `master`.")
        pipeline_ref = "master"

    if not dest_dir:
        logging.error("No destination directory given. Exiting.")
        return

    if not src_dir:
        logging.error("No source directory given. Exiting.")
        return    

    # set up gitlab connection
    config_file = os.path.join(dirname, 'python-gitlab.cfg')
    gl = gitlab.Gitlab.from_config(config_files=[config_file])
    project = gl.projects.get(project_id, lazy=True)
    pipelines = project.pipelines.list(ref=pipeline_ref)

    # find passing pipelines only
    passing_pipeline = find_passing_pipeline(pipelines)
    if not passing_pipeline:        
        logging.warning("Failed to find passing MR -- not updating docs.")
        return
    else:
        success_string = "Found passing MR with project ID: {}.".format(passing_pipeline.attributes['project_id'])
        logging.info(success_string)

    # search for artifacts from desired job
    docs_job = find_job(passing_pipeline.jobs, job_name)
    if not docs_job:
        logging.warning("Failed to find job labelled {}.".format(job_name))
        return
    else:
        logging.info("Found {}!".format(job_name))

    # download, unzip, move to dest_dir
    job = project.jobs.get(docs_job.id, lazy=True)
    get_artifacts(job, src_dir, dest_dir)

if __name__ == '__main__':
    cli()
    
    
